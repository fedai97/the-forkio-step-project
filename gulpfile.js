'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify-es').default;
const concat = require('gulp-concat');
const minifyjs = require('gulp-js-minify');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const browserSync = require("browser-sync");
const reload = browserSync.reload;

sass.compiler = require('node-sass');

gulp.task('script:vendor', function () {
    return gulp.src(
        [
            './node_modules/jquery/dist/jquery.js',
            './node_modules/@fortawesome/fontawesome-free/js/all.js'
        ]
    )
        .pipe(minifyjs())
        .pipe(uglify())
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('style:vendor', function () {
    return gulp.src(
        [
            './node_modules/reset-css/reset.css',
            './node_modules/@fortawesome/fontawesome-free/css/all.css'
        ]
    )
        .pipe(cleanCSS())
        .pipe(concat('vendor.min.css'))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('image', function () {
    return gulp.src('src/img/**/*.*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest('dist/img'))
});

gulp.task('style', function () {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('dist/css'))
        .pipe(reload({stream: true}));
});

gulp.task('script', function () {
    return gulp.src('src/js/**/*.js')
        .pipe(minifyjs())
        .pipe(uglify())
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(reload({stream: true}));
});

gulp.task('build', function () {
    return gulp.watch('src/**/*.*', gulp.series(
        'image',
        'script',
        'script:vendor',
        'style',
        'style:vendor'
    ));
});

gulp.task('sync', function () {
    return browserSync({
        server: {
            baseDir: "../the-forkio-step-project"
        },
        host: 'localhost',
        port: 8080,
        logPrefix: "Forkio"
    });
});

gulp.task('watch', function () {
    return gulp.watch('src/**/*.*', gulp.series(
        'script',
        'style'
    ));
});

gulp.task('dev', function () {
    return gulp.watch('src/**/*.*', gulp.parallel(
        'sync',
        'watch'
    ));
});

